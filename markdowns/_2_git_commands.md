# Git Commands

[Git](https://git-scm.com/) is a distrubuted version control system. It was created by Linus Trovalds, to help him manage the Linux project. It is licensed under GPLv2. Alternatives to git include [Mercurial](https://www.mercurial-scm.org/), [Apache Subversion](https://subversion.apache.org/).

---

```shell-session
~ $ cd testRepo
testRepo $ ls -a
.  ..  assets  helloworld.txt
```

### Git Init

Command used to initialize a Git repository.

```shell-session
testRepo $ git init
Initialized empty Git repository in /home/user/testRepo/.git/
```

After initializing, we can see that a new directory **.git** is created.

```shell-session
testRepo $ ls -a
.  ..  assets  .git  helloworld.txt
```

---

### Git Status

Command used to see the status...

```shell-session
testRepo $ git status
On branch main

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	assets/
	helloworld.txt

nothing added to commit but untracked files present (use "git add" to track)
```

---

### Git Add

Command used to include files. This command can be performed multiple times before a commit.

```shell-session
testRepo $ git add .
testRepo $
```

You can use **git add .** if you want to include everything in the base directory. To add only cerain files one can do **git add \<filename\>** (eg. _git add helloworld.txt assets/names.txt_ ).

Doing git status shows us which all files are waiting to be commited

```shell-session
testRepo $ git status
On branch main

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   assets/names.txt
	new file:   assets/passwords.txt
	new file:   helloworld.txt

testRepo $
```

---

### Git Commit

Time to commit our changes.

```shell-session
testRepo $ git commit -m "Initial Commit"
[main (root-commit) bd01179] Initial Commit
 3 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 assets/names.txt
 create mode 100644 assets/passwords.txt
 create mode 100644 helloworld.txt
```

Type in your commit message after **-m** flag.

Now, _git status_ will return

```shell-session
testRepo $ git status
On branch main
nothing to commit, working tree clean
```

Let's add some text to _helloworld.txt_ and commit them.

```shell-session
testRepo $ cat > helloworld.txt
HELLO WORLD
0123456789
^C
testRepo $ cat helloworld.txt
HELLO WORLD
0123456789
testRepo $ git add helloworld.txt
testRepo $ git commit -m "hello world"
[main 00cbca8] hello world
 1 file changed, 2 insertions(+)
```

---

### Git Diff

Now, let's make some modifications to _helloworld.txt_

```shell-session
testRepo $ cat > helloworld.txt
HELLO GIT
0123456789
hello world
^C
testRepo $ cat helloworld.txt
HELLO GIT
0123456789
hello world
testRepo $
```

**git diff \<filename\>** will show the changes we made to the file. Running **git diff .** will show all the changes made in the repository.

```shell-session
testRepo $ git diff helloworld.txt
diff --git a/helloworld.txt b/helloworld.txt
index f95f201..990865a 100644
--- a/helloworld.txt
+++ b/helloworld.txt
@@ -1,2 +1,3 @@
-HELLO WORLD
+HELLO GIT
 0123456789
+hello world
testRepo $
```

The **\-\-stat** flag can be used to generate a diffstat.

```console
testRepo $ git diff --stat
 helloworld.txt | 3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)
```

---

### Git Branch

Command to list all the branches.

```shell-session
testRepo $ git branch
* main
```

---

### Git Checkout

Let's create a new branch '_development_' and switch to it.

```shell-session
testRepo $ git checkout -b development
Switched to a new branch 'development'
```

The **-b** flag is used if the branch doesn't exist and we want to creat a new branch.

```shell-session
testRepo $ git branch
* development
  main
```

To switch back to the _main_ branch,

```shell-session
testRepo $ git checkout main
Switched to branch 'main'
testRepo $ git branch
  development
* main
```

---
