# Swaylock with cmatrix

### Step 1

Find the current workspace using the following command and store it in a variable

```sh
CURRENTWORKSPACE=$(swaymsg -t get_tree | grep current_workspace | cut -d'"' -f 4)
```

### Step 2

Move to an **empty** workspace, make sure that the workspace is empty. This can be done by moving to a workspace that you have not binded a key to (eg. 0, 100, -1, LOCKSCREEN, ...).

```sh
swaymsg workspace 0
```

### Step 3

Hide the statusbar, if you don't want it to show up on the screen lock.

```sh
swaymsg bar mode invisible
```

### Step 4

Run the following command to launch the terminal, execute cmatrix and lock your screen :).

```sh
swaymsg "exec alacritty -e dash -c 'sleep 0.1 && cmatrix -b'" && swaylock && killall cmatrix
```

Note: Here I am using the _Alacritty_ terminal, you can replace it with a terminal emulator of your choice. Also I am using the _dash_ shell to execute the command if you don't want to use dash, you can replace it with bash or zsh.

The **sleep 0.1** is used so the terminal can fully open before we run cmatrix. Then we run swaylock, which locks our sysytem. Once we unlock the system, we no longer want our cmatrix screen, so we killall instances of cmatrix.

### Step 5

Unhide the statusbar, if you hid it.

```sh
swaymsg bar mode toggle
```

### Step 6

Return back to where you were working before.

```sh
swaymsg "workspace "$CURRENTWORKSPACE""
```

### Step 7

(Optional) Get a **Welcome Back** notification.

```sh
dunstify "Welcome Back" -a Sway -t 2000
```

### Final

Here's how the final script looks like. (https://gitlab.com/Maskedman99/dotfiles/-/blob/master/.config/scripts/swaymatrixlock.sh)

```sh
#!/bin/dash

CURRENTWORKSPACE=$(swaymsg -t get_tree | grep current_workspace | cut -d'"' -f 4)

swaymsg workspace 0
swaymsg bar mode invisible
swaymsg "exec alacritty -e dash -c 'sleep 0.1 && cmatrix -b'" && swaylock && killall cmatrix
swaymsg bar mode toggle
swaymsg "workspace "$CURRENTWORKSPACE""

dunstify "Welcome Back" -a Sway -t 2000
```
