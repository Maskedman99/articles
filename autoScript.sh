#!/bin/sh

# Create a directory to put the converted HTML files
if [ -d page ]; then rm -r page; fi
mkdir page

# Convert the markdown files to HTML files using pandoc
find ./markdowns -iname "*.md" -type f \
  -exec sh -c 'pandoc -s --highlight-style ./public/highlightIt.theme -c ../index.css "${0}" -o "./page/$(basename ${0%.md}.html)"' {} \;

# Create a JSON file with id and title
echo "$(ls page | sed  's/_/{"id": /' | sed 's/_/, "title": "/' | sed 's/_/ /g' | sed 's/.html/"},/' | \
 tr '\n' ' ' | sed 's/{/[{/' | rev | sed 's/,}/]}/' | rev)" \
 > index.json

# Create a markdown file with list of HTML files
echo "##Blogs  " > index.md
ls page | sed 's/_/- [\/_](https:\/\/maskedman99.gitlab.io\/articles\/page\/_/' | awk -F"/_" '{print $3 $2 "/_" $3}' | cut -d '_' -f 2- | \
  sed 's/.html)//' | awk '{print "- [" $1 ")"}' >> index.md

# Use the above markdown file to create index.html file
pandoc -c index.css index.md -o index.html
